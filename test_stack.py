import unittest
import structures
from Errors import StackIsEmptyError

def createStack():
    people = structures.Stack(5)
    people.push("Kate")
    people.push("Steve")
    people.push("Abbie")
    people.push("Ella")
    return people


class TestStack(unittest.TestCase):
    def test_push(self):
        people = createStack()
        people.push("Daniel")
        self.assertEqual(people.data[people.top], "Daniel")
        with self.assertRaises(OverflowError):
            people.push("Airida")

    def test_peek(self):
        people = createStack()
        self.assertEqual(people.peek(),"Ella")


    def test_pop(self):
        people = createStack()
        people.pop()
        self.assertEqual(people.peek(), "Abbie")
        for i in range(3):
            people.pop()
        with self.assertRaises(StackIsEmptyError):
            people.pop()

    def test_isEmpty(self):
        people = createStack()
        self.assertEqual(people.isEmpty(), False)
        for i in range(people.size):
            people.pop()
        self.assertEqual(people.isEmpty(),True)

    def test_isFull(self):
        people = createStack()
        self.assertEqual(people.isFull(), False)
        people.push("Ari")
        self.assertEqual(people.isFull(),True)


if __name__ == "__main__":
    unittest.main()