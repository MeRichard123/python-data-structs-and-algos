from Errors import QueueIsEmptyError,StackIsEmptyError

class Queue:
    def __init__(self, MaxSize):
        self.MaxSize = MaxSize
        self.data = []
        self.front = len(self.data) - 1

    def __repr__(self):
        self.queue = self.data[::-1]
        return f"Queue({self.queue})"

    @property
    def size(self):
        return len(self.data)

    def enqueue(self, element):
        if len(self.data) < self.MaxSize:
            self.data.append(element)
        else:
            raise OverflowError("Queue is full")

    def dequeue(self):
        if not self.isEmpty():
            element = self.data[0]
            self.data.pop(0)
        else:
            raise QueueIsEmptyError()
        return element

    def peek(self):
        self.queue = self.data[::-1]
        return self.queue[self.front]
    
    def isEmpty(self):
        return len(self.data) == 0

    def isFull(self):
        return len(self.data) == self.MaxSize


class Stack:
    def __init__(self, MaxSize):
        self.MaxSize = MaxSize
        self.data = []
        self.top = len(self.data) - 1

    def __repr__(self):
        inverted_data = self.data[::-1]
        output = ""
        longest = inverted_data[0]
        for element in inverted_data:
            if len(element) > len(longest):
                longest = element

        for element in inverted_data:
            res = "|" + element.center(len(longest)+ 3) + "| \n"
            output += res
        
        return output

    @property
    def size(self):
        return len(self.data)

    def push(self, element):
        if len(self.data) < self.MaxSize:
            self.data.append(element)
        else:
            raise OverflowError("Stack is Full")

    def pop(self):
        if not self.isEmpty():
            element = self.data[self.top]
            self.data.pop(self.top)
            return element
        else:
            raise StackIsEmptyError
        
    def peek(self):
        return self.data[self.top]

    def isEmpty(self):
        return len(self.data) == 0

    def isFull(self):
        return len(self.data) == self.MaxSize



if __name__ == "__main__":
    pass