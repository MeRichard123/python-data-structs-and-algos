class QueueIsEmptyError(BaseException):
    pass

class StackIsEmptyError(BaseException):
    pass