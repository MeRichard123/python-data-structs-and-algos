import unittest
import structures
from Errors import QueueIsEmptyError

def createQueue():
    names = structures.Queue(5)
    names.enqueue("Kate")
    names.enqueue("Steve")
    names.enqueue("Abbie")
    names.enqueue("Ella")
    return names


class TestQueue(unittest.TestCase):

    def test_peek(self):
        names = createQueue()
        # check if peek correctly returns the first person
        self.assertEqual(names.peek(),"Kate")

    def test_enqueue(self):
        names = createQueue()
        names.enqueue("Katy")
        # Check if katy was addded by checking last
        self.assertEqual(names.data[names.front], "Katy")
        # check if error is raised if queue is full
        with self.assertRaisesRegex(OverflowError,r"Queue is full"):
            names.enqueue("Ariana")
        
    def test_dequeue(self):
        names = createQueue()
        names.dequeue()
        # check if we can remove an element from the top
        self.assertEqual(names.peek(), "Steve")
        for i in range(3): 
            names.dequeue()
        with self.assertRaises(QueueIsEmptyError):
            names.dequeue()

    def test_isEmpty(self):
        names = createQueue()
        self.assertEqual(names.isEmpty(),False)
        for i in range(names.size):
            names.dequeue()
        self.assertEqual(names.isEmpty(), True)

    def test_isFull(self):
        names = createQueue()
        self.assertEqual(names.isFull(),False)
        names.enqueue("Ari")
        self.assertEqual(names.isFull(),True)


if __name__ == "__main__":
    unittest.main()